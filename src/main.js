import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Store from '../src/store/index'
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false
// Vue.use(Vuex)
// export const store = new Vuex.Store(Store)
// Vue.use(store)

new Vue({
  router,
  store: Store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
