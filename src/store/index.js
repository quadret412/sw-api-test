// import api from '~/api'
import Vue from 'vue'
import Vuex from 'vuex'
import people from './modules/people'
import common from './modules/common'
import starships from './modules/starships'
import movies from './modules/movies'

Vue.use(Vuex)

export default new Vuex.Store({
	modules: {
		people,
		common,
		starships,
		movies,
	},
	state: {
	},
	getters: {

	},
	mutations: {
	},
	actions:{

	}
})