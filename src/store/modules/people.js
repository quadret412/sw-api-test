import api from '@/api'
import constants from '@/constants'

const state = () => ({
	currentPage: 1,
	peoplesCount: 0,
	peoplePerPage: [],
	person: {},
})
const getters = {
	showPeoplePerPage: state => Math.ceil(state.peoplesCount/constants.PER_PAGE),
}
const mutations = {
	setPeoplesCount(state, payload) {
		state.peoplesCount = payload
	},
	setPeoplePerPage(state, payload) {
		state.peoplePerPage = payload
	},
	setPerson(state, payload) {
		state.person = payload
	},
	setPage(state, payload) {
		state.currentPage = payload
	},
}
const actions = {
	async setCurrentPerson({commit}, id) {
		try {
			commit('common/setLoading', null, { root: true })
			const response = await api.getSeparatePerson(id)
			await commit('setPerson', response)
			await commit('common/endLoading', null, { root: true })
		} catch (e) {
			throw new Error(e)
		}
	},
	async getPeoplesCards({ commit, getters, state }, page) {
		try {
			if(state.peoplesCount !== 0 && (page === 0 || page > getters.showPeoplePerPage)) {
				return false
			}
			await commit('common/setLoading', null, { root: true })
			await commit('setPage', page)
			const response = await api.getMainPeople(page)
			await commit('setPeoplesCount', response.count)
			await commit('setPeoplePerPage', response.results)
			await commit('common/endLoading', null, { root: true })
		} catch (e) {
			throw new Error(e)
		}

	}
}

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}