import api from "@/api";

const state = () => ({
})
const getters = {

}
const mutations = {
}
const actions = {
	async getMoviesNames({commit}, arrayOfIds) {
		try {
			commit('common/setLoading', null, { root: true })
			let tempArray = []
			for (const id of arrayOfIds) {
				const film = await api.getMoviesData(id)
				tempArray.push(film.title)
			}
			commit('common/endLoading', null, { root: true })
			return tempArray
		} catch (e) {
			throw new Error(e)
		}
	},
}

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}