import api from "@/api";

const state = () => ({
})
const getters = {

}
const mutations = {
}
const actions = {
	async getStarshipsNames({commit}, arrayOfIds) {
		try {
			await commit('common/setLoading', null, { root: true })
			let tempArray = []
			for (const id of arrayOfIds) {
				const ship = await api.getStarshipData(id)
				tempArray.push(ship.name)
			}
			commit('common/endLoading', null, { root: true })
			return tempArray
		} catch (e) {
			throw new Error(e)
		}
	},
}

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}