const state = () => ({
	isLoading: false,
})
const getters = {

}
const mutations = {
	setLoading(state) {
		state.isLoading = true
	},
	endLoading(state) {
		setTimeout(() => {
			state.isLoading = false
		}, 500)

	},
}
const actions = {
}

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}