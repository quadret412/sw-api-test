import axios from 'axios'

const axiosInstance = axios.create({
	baseURL: 'https://swapi.dev/api/',
	// withCredentials: true,
	// headers: {
	// 	'Accept': 'application/json',
	// 	'X-Requested-With': 'XMLHttpRequest',
	// },
})

export default {
	getMainPeople(page) {
		return axiosInstance.get(`people/?page=${page}`).then(r => r.data)
	},
	getSeparatePerson(id) {
		return axiosInstance.get(`people/${id}/`).then(r => r.data)
	},
	getStarshipData(id) {
		return axiosInstance.get(`starships/${id}/`).then(r => r.data)
	},
	getMoviesData(id) {
		return axiosInstance.get(`films/${id}/`).then(r => r.data)
	},
}
